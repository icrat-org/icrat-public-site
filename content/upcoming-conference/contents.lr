title: The 10th International Conference
---
body: 

<div style="max-width:75%" class="center-block">
<img src="./USF-COE.jpg" class="img-responsive"/>
</div>

<br/>

Following the success of its nine previous editions, the United States Federal Aviation Administration and EUROCONTROL are jointly organizing the 10th edition of the International Conference on Research in Air Transportation (ICRAT), which is to be held June 19-23, 2022, at the University of South Florida in Tampa, Florida, USA. It is the hope of the ICRAT planning committee that the conference can be held with full in-person participation. The pandemic situation might require a transition to a hybrid or virtual model; the latest information will be available on the ICRAT 2022 web site.

<div style="font-size: 20px;" class="alert alert-secondary" role="alert">
ICRAT 2022 Schedule <a class="alert-link" href="./ICRAT_2022_Schedule.pdf">(PDF)</a> <a class ="alert-link" href="./ICRAT_2022_Schedule.xlsx">(XLSX)</a> now available! &raquo;</a><br/>
<a class="alert-link" href="./Abstracts_ICRAT2022.pdf">ICRAT 2022 Book of Abstracts (PDF) now available! &raquo;</a> <br/>
<a class="alert-link" href="./registration/">Registration form is now open! &raquo;</a> <br/>
<a class="alert-link" href="./sponsorship/">Sponsorship Opportunities now available! &raquo;</a> <br/>
</div>

* [Registration Form](./registration/)
* [Registration Information (PDF)](./Registration_Information.pdf)
* [ICRAT 2022 Book of Abstracts(PDF)](./Abstracts_ICRAT2022.pdf)
* ICRAT 2022 Schedule [(PDF)](./ICRAT_2022_Schedule.pdf)  [(XLSX)](./ICRAT_2022_Schedule.xlsx)
* [Hotel Information](./hotel/)
* [Tampa Area Local Attractions](./local-attractions/)
* [Program At A Glance (PDF)](./Program-At-A-Glance.pdf)
* [Committee Members](./committee-members/)
* [Call for Papers](./call-for-papers/)
* [Call for Papers (PDF)](./call-for-papers/ICRAT2022_Call_for_Papers.pdf)
* [Regular Paper Template (docx, updated 8-Feb-2022)](./call-for-papers/ICRAT2022_regular_paper_template.docx)
* [Doctor Symposium Template (docx, updated 27-Jan-2022)](./call-for-papers/ICRAT2022_doctoral_symposium_template.docx)

